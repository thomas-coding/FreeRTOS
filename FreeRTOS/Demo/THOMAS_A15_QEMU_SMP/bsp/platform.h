/*
 * Header for platform.
 *
 * Copyright (C) 2021 VeriSilicon Holdings Co., Ltd.
 *
 */

#ifndef _PLATFORM_H__
#define _PLATFORM_H__

void platform_init(void);

#endif /* __ALIUS_PLATFORM_H__ */
