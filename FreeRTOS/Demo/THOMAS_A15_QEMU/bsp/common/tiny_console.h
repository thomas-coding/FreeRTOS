/*
 * Copyright (c) 2021-2031, Jinping Wu. All rights reserved.
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef TINY_CONSOLE_H
#define TINY_CONSOLE_H

void tiny_uart_console(void);

#endif
